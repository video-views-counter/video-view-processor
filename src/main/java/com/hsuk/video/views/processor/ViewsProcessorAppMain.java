package com.hsuk.video.views.processor;

import com.hsuk.video.views.processor.spark.VideoViewStreamProcessor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ViewsProcessorAppMain {

    public static void main(String[] args) {
        final VideoViewStreamProcessor videoViewStreamProcessor = new VideoViewStreamProcessor();
        log.info("Starting video view processing Job...");

        videoViewStreamProcessor.process(args);

    }

}
