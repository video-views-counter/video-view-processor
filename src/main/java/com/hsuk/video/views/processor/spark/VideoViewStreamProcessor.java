package com.hsuk.video.views.processor.spark;

import com.hsuk.video.views.processor.cache.PropertiesCache;
import com.hsuk.video.views.processor.constants.AppConstants;
import com.hsuk.video.views.processor.kafka.deserializers.VideoViewDeserializer;
import com.hsuk.video.views.processor.model.VideoView;
import com.hsuk.video.views.processor.rabbitmq.MessageSender;
import com.hsuk.video.views.processor.spark.DeDuplicationProcessor;
import com.hsuk.videoviewerconsumer.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;

import java.util.*;

@Slf4j
public class VideoViewStreamProcessor {

    private static final Iterator<String> EMPTY_ITERATOR = new ArrayList<String>(0).iterator();
    private final DeDuplicationProcessor deDuplicationProcessor = new DeDuplicationProcessor();
    private final MessageSender messageSender = new MessageSender();
    private final static DateUtils dateUtils = new DateUtils();

    public void process(String[] args) {
        SparkConf conf = new SparkConf().setAppName("videoviews-kafka-stream").setMaster("local[*]");
        System.setProperty("hadoop.home.dir", "/");
        System.setProperty("SPARK_KAFKA_VERSION", "0.10");

        try (JavaStreamingContext ssc = new JavaStreamingContext(new JavaSparkContext(conf), Durations.seconds(
                Long.parseLong(PropertiesCache.getInstance().getProperty(AppConstants.KAFKA_STREAM_INTERVAL_IN_SECONDS))))) {

            JavaInputDStream<ConsumerRecord<String, VideoView>> stream =
                    KafkaUtils.createDirectStream(
                            ssc,
                            LocationStrategies.PreferConsistent(),
                            ConsumerStrategies.<String, VideoView>Subscribe(Collections.singletonList(PropertiesCache.INSTANCE.getProperty(AppConstants.KAFKA_VIDEOVIEW_TOPICS)),
                                    createConsumerProperties(args))
                    );

            // Read value of each message from Kafka and return it
            JavaDStream<VideoView> messages = stream
                    .map((Function<ConsumerRecord<String, VideoView>, VideoView>) record -> {
                        log.info("Received message from Kafka: {}", record.value());
                        return record.value();
                    });

            // unique check.
            // kafka does not guarantees only once delivery.
            // To ensure count is accurate duplicate check is applied here using redis cache and only unique are sent in the pipeline
            JavaDStream<VideoView> uniqueStream = deDuplicationProcessor.process(messages);

            // Break every message into VideoView and return list of video ids
            JavaDStream<String> videoIds = uniqueStream
                    .flatMap((FlatMapFunction<VideoView, String>)
                            videoView -> {
                                if (videoView != null) {
                                    String suffix = dateUtils.getSuffix(videoView.getTimestamp());
//                                    return Collections.singletonList(String.valueOf(videoView.getVideoId())).iterator();
                                    return Collections.singletonList(videoView.getVideoId() + "_" + suffix).iterator();
                                } else {
                                    return EMPTY_ITERATOR;
                                }
                            });

            // Take every videoId and return Tuple with (videoId,1)
            JavaPairDStream<String, Integer> videoViewCounterMap = videoIds
                    .mapToPair((PairFunction<String, String, Integer>)
                            videoId -> {
                                return new Tuple2<>(videoId, 1);
                            });

            // Count occurrence of each videoId
            JavaPairDStream<String, Integer> videoViewCount = videoViewCounterMap
                    .reduceByKey(
                            (Function2<Integer, Integer, Integer>) Integer::sum);
            messageSender.publishToRabbitMQ(videoViewCount);

            videoViewCount.print();
            ssc.start();
            ssc.awaitTermination();

        } catch (Exception ex) {
            log.error("Error runnig Spark Job for Video view processor. {}", ex.getMessage());
        }

    }

    private Map<String, Object> createConsumerProperties(String[] args) {
        Map<String, Object> consumerProperties = new HashMap<>();

        consumerProperties.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, "org.apache.kafka.clients.consumer.RangeAssignor");
        consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, PropertiesCache.getInstance().getProperty(AppConstants.KAFKA_BOOTSTRAP_SERVERS_CONFIG));
        consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, PropertiesCache.getInstance().getProperty(AppConstants.KAFKA_GROUP_ID_CONFIG));
        consumerProperties.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, PropertiesCache.getInstance().getProperty(AppConstants.KAFKA_REQUEST_TIMEOUT_MS_CONFIG));
        consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, PropertiesCache.getInstance().getProperty(AppConstants.KAFKA_AUTO_OFFSET_RESET_CONFIG));
        consumerProperties.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, PropertiesCache.getInstance().getProperty(AppConstants.KAFKA_AUTO_COMMIT_INTERVAL_MS_CONFIG));
        consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, VideoViewDeserializer.class);
        consumerProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");

        return addAdditionalConfigs(args, consumerProperties);
    }

    private Map<String, Object> addAdditionalConfigs(String[] args, Map<String, Object> consumerProperties) {
        for (String arg : args) {
            int i = arg.indexOf('=');
            if (i > 0) {
                String key = arg.substring(0, i);
                String value = arg.substring(i + 1);
                consumerProperties.put(key, value);
                log.info("key={}, value={}", key, value);
                log.info(key + "=" + value);
            }
        }

        return consumerProperties;
    }

}