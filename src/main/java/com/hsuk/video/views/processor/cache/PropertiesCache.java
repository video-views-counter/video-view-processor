package com.hsuk.video.views.processor.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum PropertiesCache {
    INSTANCE;
    private final Properties configProp = new Properties();
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    PropertiesCache() {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties");
        try {
            configProp.load(in);
        } catch (IOException e) {
            log.error("Error reading property file", e);
        }
    }

    public static PropertiesCache getInstance() {
        return INSTANCE;
    }

    public String getProperty(String key) {
        return configProp.getProperty(key);
    }
}